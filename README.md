TODO: restore code from local machine.

Setup: backend server runs socket.io allowing one computer or device to connect 
as the "host" and multiple others to connect as "displays". The host machine
will require a webcam and will direct the displays to shows a unique ID in a
large font on their screens so that it can determine their positions in 2D
space. Large changes in acellerometer data of any of the display devices will
trigger the host to "relocate" that screen. Small movements can be accounted for
without image detection but after device has been still for more than a second
the host machine can recheck it's location (account for dead-reckoning).

